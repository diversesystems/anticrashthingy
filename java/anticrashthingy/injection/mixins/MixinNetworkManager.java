package anticrashthingy.injection.mixins;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.DecoderException;
import net.minecraft.client.Minecraft;
import net.minecraft.network.NetworkManager;
import net.minecraft.util.text.TextComponentString;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.io.IOException;

@Mixin(NetworkManager.class)
public class MixinNetworkManager {
    @Inject(method = "exceptionCaught", at = @At("HEAD"), cancellable = true)
    private void exceptionCaught(ChannelHandlerContext p_exceptionCaught_1_, Throwable p_exceptionCaught_2_, CallbackInfo info) {
        if (p_exceptionCaught_2_ instanceof IOException) {
            info.cancel();
            Minecraft.getMinecraft().player.sendMessage(new TextComponentString("Possible Ingame DDOS/Crasher detected"));
            return;
        }

        if (p_exceptionCaught_2_ instanceof DecoderException) {
            info.cancel();
            Minecraft.getMinecraft().player.sendMessage(new TextComponentString("Possible Ingame DDOS/Crasher detected"));
        }

        if (p_exceptionCaught_2_ instanceof IndexOutOfBoundsException) {
            info.cancel();
            Minecraft.getMinecraft().player.sendMessage(new TextComponentString("Possible Ingame DDOS/Crasher detected"));
        }
    }
}
