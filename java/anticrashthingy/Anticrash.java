package anticrashthingy;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

@Mod(modid = "anticrashthingy", name = "anticrashthingy", version = "1.0")
public class Anticrash {

    @Mod.EventHandler
    public void onInit(FMLInitializationEvent e){
        MinecraftForge.EVENT_BUS.register(this);
        FMLCommonHandler.instance().bus().register(this);
        System.out.println("AntiCrasher load");
    }

}
